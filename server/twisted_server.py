import logging

from twisted.internet import reactor
from twisted.internet.protocol import DatagramProtocol

from server import config

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class EchoUDP(DatagramProtocol):
    def datagramReceived(self, datagram, addr):
        """Datagram received."""
        logger.debug(
            'Datagram "%s" received from %s', datagram.decode("utf-8"), repr(addr)
        )
        self.transport.write(datagram, addr)


def main():
    """Run application."""
    # Get config items
    host = config.get("default", "host")
    port = config.getint("default", "port")

    logger.debug("Listening on %s:%d..." % (host, port))
    reactor.listenUDP(port, EchoUDP())
    reactor.run()

    logger.info("Server shut down gracefully")


if __name__ == "__main__":
    main()
