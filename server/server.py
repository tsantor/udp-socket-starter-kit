# -*- coding: utf-8 -*-

import logging
import signal
import socket

from server import config

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------

running = True

def _handle_shutdown_signals(signum, frame):
    assert signum in (signal.SIGINT, signal.SIGTERM)
    logger.info("signal -%d received", signum)
    logger.info("Received graceful shutdown request")
    global running
    running = False


def main():
    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    signal.signal(signal.SIGINT, _handle_shutdown_signals)
    signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get("default", "host")
    port = config.getint("default", "port")

    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the socket to the port
    server_address = (host, port)
    # print ('starting up on %s port %s' % server_address)
    logger.debug("Listening on %s:%d..." % (host, port))
    sock.bind(server_address)

    while running:
        # try:
        logger.debug("waiting to receive message")
        data, address = sock.recvfrom(4096)

        logger.debug("received %s bytes from %s" % (len(data), address))
        logger.debug(data)

        if data:
            sent = sock.sendto(data, address)
            logger.debug("sent %s bytes back to %s" % (sent, address))
        # except socket.error as e:
        #     logger.error(str(e))
        #     # if e.errno != errno.EINTR:
        #     raise
        # else:
        #     logger.debug('break while loop')
        #     break

    logger.info("Server shut down gracefully")


if __name__ == "__main__":
    main()
