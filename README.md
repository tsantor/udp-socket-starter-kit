# UDP Socker Starter Kit


## Setup development environment
As a developer I assume you have [pyenv](https://github.com/pyenv/pyenv) installed.

```bash
make env
make reqs
```

## Run examples
### Server
```bash
make serve
```

### Client
```bash
make listen
```
