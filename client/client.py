# -*- coding: utf-8 -*-

import logging
import socket

from client import config

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


# def _handle_shutdown_signals(signum, frame):
#     assert signum in (signal.SIGINT, signal.SIGTERM)
#     logger.info('signal -%d received', signum)
#     logger.info('Received graceful shutdown request')


def main():
    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    # signal.signal(signal.SIGINT, _handle_shutdown_signals)
    # signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get("default", "host")
    port = config.getint("default", "port")

    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    server_address = (host, port)
    message = b"This is the message.  It will be repeated."

    try:

        # Send data
        logger.debug('sending "%s"' % message)
        sent = sock.sendto(message, server_address)

        # Receive response
        logger.debug("waiting to receive")
        data, server = sock.recvfrom(4096)
        logger.debug('received "%s"' % data)

    finally:
        logger.debug("closing socket")
        sock.close()


if __name__ == "__main__":
    main()
