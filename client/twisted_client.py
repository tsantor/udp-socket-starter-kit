# -*- coding: utf-8 -*-

from __future__ import print_function

import logging
import socket

# 3rd party
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

# App
from client import config

LOGGER = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class EchoClientDatagramProtocol(DatagramProtocol):
    strings = [
        "Hello, world!",
        "What a fine day it is.",
        "Bye-bye!"
    ]

    def startProtocol(self):
        """Connect to server."""
        # Get config items
        host = config.get('default', 'host')
        port = config.getint('default', 'port')

        self.transport.connect(host, port)
        self.sendDatagram()

    def sendDatagram(self):
        """Send datagram."""
        if self.strings:
            datagram = self.strings.pop(0)
            self.transport.write(datagram.encode('utf-8'))
        else:
            reactor.stop()

    def datagramReceived(self, datagram, addr):
        """Datagram received."""
        LOGGER.debug('Datagram %s received from %s',
                      repr(datagram), repr(addr))
        self.sendDatagram()

    def connectionRefused(self):
        """Possibly invoked if there is no server listening on the
        address to which we are sending."""
        LOGGER.warning("No one listening")
        reactor.stop()

    def connectionFailed(self, failure):
        """Called if connecting failed. Usually this will be due to a
        DNS lookup failure."""
        LOGGER.warning("Connection failed")


def main():
    """Run application."""
    # Get config items
    # host = config.get('default', 'host')
    # port = config.getint('default', 'port')

    protocol = EchoClientDatagramProtocol()

    try:
        # logger.debug('Listening on %s:%d...' % (host, port))
        # 0 means any port, we don't care in this case
        t = reactor.listenUDP(0, protocol)
        reactor.run()
    except socket.error as e:
        LOGGER.error(str(e))

    LOGGER.info('Client shut down gracefully')


if __name__ == '__main__':
    main()
