env:
	pyenv virtualenv 3.9.4 udpsocket_env && pyenv local udpsocket_env

reqs:
	python -m pip install -U pip black pylint
	# pre-commit && pre-commit install
	python -m pip install -r requirements.txt

serve:
	cd server && python twisted_server.py

listen:
	cd client && python twisted_client.py
